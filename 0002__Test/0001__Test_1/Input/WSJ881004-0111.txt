1984 summer olympic games even advertisers began clamoring pixie gymnast mary lou retton 
went grin relentlessly 5 million worth commercial endorsements surely olympic record 
1988 summer games ended sunday night chance american athletes follow footsteps 
ad agency executives spend every fourth year glued tv sets hoping pick next star hawk clients products say olympiad dud 
suspect go dullest history olympics says jerry della femina chairman ad agency della femina mcnamee wcrs 
first olympics without passion without passion testimonials 
without heroes anyone anyone wants emulate 
surface games seemed offer drama usually makes lucrative endorsement contracts 
could forget diver greg louganis smacking head springboard coming back win two gold medals 
flamboyant florence griffith joyner two-inch fingernails raking air 100-meter sprint 
even two along jackie joyner-kersee women heptathalon long jump may find difficult previous years turn gold medals gold bank 
fact say many marketers olympic gold lost much commercial luster 
think big money like used says michael goldberg national media group inc. new york sports-marketing firm represents ms. retton 
used kind knee-jerk reaction ad campaign around olympic athlete 
happen anymore 
certainly year winning olympians trouble securing contracts specialized sports-equipment outfitters speaking invitations obscure trade groups 
major high-paying endorsements soft-drink companies fast-food outlets seem surfacing least yet 
earlier year sports marketers estimated gold medal summer games could earn personable athlete 1 million endorsements 
marketers say medalists lucky pull 100,000 500,000 year 
advertisers reel litany reasons olympics igniting much spark 
one thing suffer comparison 1984 summer games exciting held los angeles absence boycotting soviet-bloc countries americans 83 gold medals 
1988 games contrast broadcast halfway around world soviets trounced americans 55 gold medals 36 
marketers say year drug scandals cast pall games especially incident involving canadian sprinter ben johnson lost gold medal testing positive steroids 
nbc tv coverage also gets share blame 
marketers say bland skipped frequently one event another little chance excitement build 
constant commercial interruptions led ratings much lower expected help either 
past games enough continuous tv coverage much like soap opera possible get involved lives competition says leigh steinberg attorney ice-skater brian boitano gold medalist 1988 winter games 
summer says 
recent changes olympic rules meanwhile led glut olympians search endorsement deals 
1984 games olympic athletes allowed make commercials unless gave amateur status 
appear many ads want income must go trust fund training expenses 
every year olympic people wanting commercials basically everybody available says daisy sinclair vice president ad agency ogilvy group inc 
result aura surrounded olympian bloom 
companies also becoming sophisticated approach celebrity endorsements 
increasingly cost conscious longer hook olympian company president likes athlete sport 
years paying big bucks olympic stars see fade obscurity companies finally gotten hint olympian fame fleeting variety 
think phone ring hook win gold medal going happen anymore says stephen disson senior vice president marketing proserv inc. washington sports-marketing firm 
companies know six months heroes sporting events consider 
ad agency bbdo worldwide attitude toward olympics becoming typical 
uses sports stars commercials pepsi light oppenheimer mutual funds plans call 1988 olympians 
prefer use athletes public eye regular basis like football basketball players says charles miesmer agency executive creative director 
people like michael jordan 10-plus years sports careers row worry public saying 'carl 
look 1988 winners start showing wheaties boxes either 
general mills inc. featured ms. retton decathalon gold-medalist bruce jenner wheaties boxes past says looks three qualities endorser career achievement popular appeal off-the-field accomplishments 
spokeswoman says none year crop meet three criteria 
far ms. joyner-kersee visible endorsements 
even olympics began made commercials primatene mist 7-up 
according agent pearl hodge world class management process signing mcdonald corp. work behalf efforts aid underprivileged children 
ms. joyner-kersee sister-in-law ms. griffith joyner may soon take endorsement lead 
advertisers say good looks fashion flare attracting interest medal winners 
sprinter manager could reached comment 
another olympian endorsement potential swimmer matt biondi five gold medals 
mr. biondi already signed swim-wear contract arena usa inc. parkes brittain agent advantage international inc. says client definitely high-income ring highest name 
mr. louganis 
since winning two gold medals 1984 summer olympics diver little luck major endorsements 
marketing executives say even second sweep diving events may potential 
advertisers looking male hero word 'macho trails along closely says marty blackman blackman raber sports-marketing firm new york 
find louganis macho guy 
still marketing professionals say mr 
louganis dramatic recovery smashing head secured diver future 
manager jim babbitt wo comment contract offers 
say however client done fine 
unhappy 
dives perform dive dollars 
winning endorsements one former olympian among top sports endorsers last year 
athlete arnold palmer golf national endorsements hertz painewebber pennzoil united airlines gte toro loft seed co. sears pro group robert bruce westin hotels estimated 1987 earnings 8 million athlete jack nicklaus golf national endorsements bostonian shoe co. hart schaffner marx optique du monde pine hosiery mills warnaco estimated 1987 earnings 6 million athlete boris becker tennis national endorsements puma coca-cola deutsche bank polaroid unicef estimated 1987 earnings 6 million athlete greg norman golf national endorsements reebok spalding epson computers hertz qantas airlines daikyo group swan lager castlemaine akubia hats estimated 1987 earnings 4.5 million athlete michael jordan basketball 1984 olympian national endorsements nike wilson excelsior mcdonald coca-cola chevrolet johnson products ohio art estimated 1987 earnings 4 million athlete ivan lendl tennis national endorsements gleneagles country club avis seiko adidas bow brand strings estimated 1987 earnings 3 million athlete john madden former football coach national endorsements ace hardware miller lite ramada exxon canon greyhound estimated 1987 earnings 3 million athlete jim mcmahon football national endorsements taco bell kraft foods ebel watches honda adidas ljn toys revo sunglasses estimated 1987 earnings 3 million athlete dennis conner yachting national endorsements rayban sperry topsider wrigley disney lacoste pepsi-cola estimated 1987 earnings 2 million athlete chris evert tennis national endorsements lipton iced tea wilson converse ellesse polo club boca raton rolex evert active life skin care marchon sunwear ellesse estimated 1987 earnings 2 million athlete martina navratilova tennis national endorsements puma lufthansa airlines vuarnet dhl sugar-free jello aspen club rko video world tennis magazine estimated 1987 earnings 2 million athlete jackie stewart retired race car driver national endorsements ford goodyear usf g rolex gleneagles american express british telecom icci estimated 1987 earnings 2 million source sports marketing newsletter 
