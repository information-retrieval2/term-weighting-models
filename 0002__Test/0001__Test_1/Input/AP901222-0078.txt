chief petty officer lance vickery recalls waves tumbling stern bobbing ferry headed back uss saratoga joking whoa looks like 're flooding 
minutes later words came true turning simple ride back christmas shore leave harrowing fight survive 
israeli ferry tuvia capsized shortly midnight saturday throwing 100 men cold waters haifa bay 
least 19 sailors drowned scores injured 
sailors part multinational force arrayed iraq 
vickery 33 among four american sailors interviewed haifa rambam hospital 
jacksonville fla. far mayport saragota home port 
three sailors said boat taking water stern heavy seas capsized hit big wave 
vickery said fatal wave came minute cracked joke 
sudden saw water come flying said 
maybe two seconds boat rolled right 
13-year veteran sailor found trapped ferry cabin frantically struggling find window ship sank 
started breathing water almost died vickery said 
water maybe another 30 seconds would lying right 
'd one body bags 
made way surface found guys floating everywhere screaming yelling like chaos 
despite predicament grabbed struggling fellow sailor collar dragged toward floating plastic foam life ring 
one rescue boat already overloaded passed threw life preservers 
seemed like hour another boat came also full 
dragged us water back saratoga real happy person see guys snatching said 
vickery treated bleeding lungs caused water inhalation hospital roommate seaman michael benjamin 19 san antonio texas recovering neck injury suffered rescue boat hit water 
wearing blue hospital pajamas walked visiting room balcony could see saratoga anchored outside haifa harbor 
benjamin navy year said ferry jostled heavy seas 
going waves noticed one bigger normal another one bigger normal said 
next thing know saw coming water said 
benjamin said dove ferry swim frantically get away suction sinking boat 
clinging foam plastic float passed one rescue boat hit second 
looked around see big hull coming yelling guess ship see us said 
trying get way got hit ship 
dazed benjamin stripped jacket shirt shoes kept treading water found piece driftwood cling 
scared admitted 
hard time staying water 
rescue ship picked estimated half hour 45 minutes later 
two men interviewed rambam hospital identified technician john richards 31 twin lake mich. airman lamont jones 20 claremont va 
breathing difficulty despite gas masks 
richards recalled seeing large wave feeling ship heeling one side 
remember thinking oh god sinking scrambling life said faintly 
