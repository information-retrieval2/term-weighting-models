new york philharmonic supposed night beethoven shostakovich 
fates made night leonard bernstein 
official catalog works says bernstein led 74 orchestras 
orchestra musicians came pay tribute laureate conductor led 1,244 concerts 47-year association philharmonic 
let music talking 
tonight concert evening want dwell passing giant rather left us guest conductor leonard slatkin told audience avery fisher hall thursday night 
four nights bernstein death age 72 beethoven violin concerto became bernstein serenade solo violin string orchestra harp percussion 1954 shostakovich symphony 4 became bernstein symphony 1 jeremiah written 1942 
also added chichester psalms 1965 overture candide 1956 
substitute bernstein program repeated friday night saturday tuesday 
extraordinary concert 
many smiling faces among audience musicians 
slatkin brief introductory comments followed whimsical candide broke tension 
came eclectic serenade bernstein program notes says supposed based plato symposium 
begins lyrical praise eros god love 
piece sometimes jocular sometimes melancholy sometimes assaulting sometimes hedonistic 
ends drunken revelry intrusion jazz bernstein looking heaven telling audience o.k smile tonight 
soloist concertmaster glenn dicterow played much agility especially difficult double stops especially considering lack preparation time 
mourning returned intermission without optimism 
jeremiah begins suspenseful strings followed cantorial french horn melodic return strings prophetic blaring brass 
second movement continues point-counterpoint brass versus strings jeremiah versus corrupt priests nebuchadnezzar looming horizon 
third movement jerusalem fallen babylonian king 
judea exiled 
jeremiah weeps 
sits desolate city full people become widow 
chanting prophet lamentations mezzo-soprano wendy white wipe tear eye 
members audience 
last program another hebraic elegy chichester psalms 
new york choral artists boy soloist joined orchestra 
two harpists dominated center crowded stage 
awake psaltery harp choir begins hebrew 
rouse dawn 
second movement angelic 8th-grader evan weber sings king david 23rd psalm lord shepherd shall want 
finishes men chorus bellows nations rage 
piece however ends mahlerian motif chorus declaring message bernstein sought society behold good pleasant brethren dwell together unity 
emphasize point final note slatkin suspended hands several long moments silence 
came cascade applause 
leonard bernstein 
