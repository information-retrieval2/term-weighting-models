mikhail s. gorbachev friday proposed tougher legislation punish soviets seek kindle ethnic unrest saying nationalist passions benefit opponents reforms 
soviet leader told meeting communist party leadership inter-ethnic relations worsening regions country due part active resistance put perestroika corrupt groups 
perestroika restructuring gorbachev program widespread economic social change 
state-run media also indicated friday crackdown begun people armenia continue agitate annexation mostly armenian enclave neighboring republic azerbaijan 
six-month conflict two republics control nagorno-karabakh district led mass rallies strikes ethnic violence since february 
supreme soviet presidium government top executive body intervened july 18 
rejected change nagorno-karabakh status called measures develop region economy culture 
conflict nagorno-karabakh one rancorous domestic disputes faced gorbachev since became communist party chief march 1985 
indicated friday wants defuse tensions ethnic groups also punish transform tensions political crisis 
gorbachev spoke plenary meeting central committee party policy-making body 
summary remarks distributed official tass news agency 
gorbachev outlined reasons current aggravation tensions ethnic groups regions soviet union tass said 
among causes gorbachev listed lack concern many years specific requirements peoples ethnic groups inadequate control masses activities executive personnel active resistance put perestroika corrupt groups 
tass said gorbachev noted nationalist passions benefit anti-perestroika forces suggested increasing accountability soviet laws kindling ethnic strife 
tass give details gorbachev suggestion 
july 18 presidium meeting soviet leader accused corrupt conservatives oppose reforms hiding behind nagorno-karabakh annexation drive 
gorbachev said grounds tougher legislation come directly article 36 1977 soviet constitution says law punish advocacy racial national exclusiveness hostility contempt 
tass said gorbachev made proposals resolve ethnic tensions including increase power country 15 republics law equal use 100 languages spoken soviet union economic autonomy country republics regions 
particular baltic republics lithuania latvia estonia forcibly incorporated soviet union 1940 demanded say economic government affairs 
labor newspaper trud said 44 people yerevan armenia capital face criminal charges following incident activists seeking annexation nagorno-karabakh attached slogans cars drove around city 
paper give date size mobile demonstration said 155 drivers licenses revoked 310 fined 
july 18th decision presidium also ordered local law enforcement agencies ensure order restored caucasus mountain region 
tass reported friday situation stabilizing armenia said urging instigators strikes protests continuing 
