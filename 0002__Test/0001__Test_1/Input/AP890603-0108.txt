look key events student pro-democracy movement led sunday assault soldiers protesters tiananmen square april 15 former communist party chief hu yaobang dies 
beijing university students put posters praising indirectly criticizing opponents forced resignation student demonstrations 1986-87 
april 17 thousands students march beijing shanghai shouting long live hu yaobang 
long live democracy april 27 150,000 students wide support people street surge past police lines fill tiananmen square chanting slogans democracy freedom 
april 29 officials meet student leaders independent student groups satisfied continue class boycott 41 universities 
april 30 beijing communist party chief zhao ziyang meets student representatives 
may 4 100,000 students supporters march tiananmen square celebrate 70th anniversary china first student movement 
demonstrations held shanghai nanjing cities 
300 journalists protest outside official xinhua news agency 
may 9 journalists petition government press freedom 
may 10 thousands students bicycle major media offices press freedom demonstration 
another protest held provincial capital taiyuan 
may 13 2,000 students begin hunger strike tiananmen square 
may 14 thousands flock square back students 
number fasting rises 3,000 
may 15 government deadline students leave square passes 
soviet president mikhail 
gorbachev welcoming ceremony planned near square must moved airport 
may 16 hundreds thousands occupy square 
journalists intellectuals join protest 
may 17 zhao pleas students leave 
students reject appeal hold marches generate 1 million supporters beijing 
may 18 1 million people including many workers take streets show support hunger strikers 
li peng issues stern lecture student leaders refuses discuss demands 
may 19 tearful zhao visits weakened hunger strikers 
li also visits students briefly 
students later decide end hunger strike 
may 20 martial law declared parts beijing 
zhao reportedly resigns failing persuade li hardliners compromise 
students resume hunger strike abandon saying need strength struggle ahead 
may 21 students claim reject order li peng leave tiananman square face military action 
defying martial law hundreds thousands people remain square block intersections prevent troops reaching 
may 22 hundreds thousands people block major roads senior military leaders say resisting orders order troops protesters 
least one military convoy reported withdraw 
may 23 one million people take streets demonstrate democracy 
may 25 li makes first appearance since declaring martial law 
state- run radio says 27 29 provinces support martial law military units announce support martial law 
may 26 sources say zhao another leading liberal wan li head legislature house arrest 
may 27 beijing student leaders propose students end occupation tiananmen square 
non-beijing students resist sit-in continues 
may 28 80,000 people many students outside capital stage demonstration unlike past rallies workers participate 
may 30 students unveil goddess democracy replica statue liberty square 
government calls insult nation 
students rally outside police headquarters protest detention three members independent labor union 
may 31 first several pro-government rallies staged farmers workers beijing suburbs 
participants ordered show show little enthusiasm 
june 1 beijing municipal government warns foreign journalists martial law must receive approval press coverage 
coverage demonstrations banned 
june 2 1,000 troops appear outside railway station show force 
june 3-4 tens thousands troops make several attempts move square driven back crowds hundreds thousands people supporting students 
riot squads beat fire tear gas demonstrators 
final push made evening june 3 soldiers begin firing crowds 
troops surround enter square june 4 early morning hours thousands protesters flee 
hundreds wounded many dead 
