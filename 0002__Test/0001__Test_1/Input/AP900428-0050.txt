hubble space telescope awakened self-induced sleep today still without use high-speed antennas science instruments hold unable begin observations 
safe-mode right said steve terry supervisor engineers controlling telescope 
said telescope running normal control-all mode computers sensors guide pointing 
discovery astronauts meanwhile packed gear return earth sunday made last-minute observations earth talked ground-based reporters flight 
pilot charles bolden said especially upbeat widespread public awareness telescope expected eventually look back 14 billion light years nearly birth universe 
hard go somewhere find someone never heard hubble space telescope said bolden 
makes little kids eyes light 
makes little kids want learn add subtract study science 
telescope big eye remained open hubble safe mode motion stopped earlier problems linking high-speed dish antennas relay satellite 
antennas primary links relaying scientific data ground 
terry said even safe mode one tape recorders telescope reads telemetry signals systems would played earth analyzed antennas working properly 
expect use data investigate happened said 
engineers able send commands telescope via secondary antennas communications system useful scientific data must transmitted high rate speed 
're putting scientific instruments hold mode effect standby mode said 
shortly 6 a.m. edt controllers goddard space flight center greenbelt md. completed series commands took telescope safe mode began run 12-hour computer program monitor hubble health 
take another day 1.5 billion telescope operating way supposed point startup process said hubble deputy project manager jean olivier 
're backing deliberate slow careful approach problems olivier said friday 
time anything dumb 
telescope problems put flight center several days behind five astronauts deployed earlier week sticking schedule 
landing scheduled 9:48 a.m. edt sunday edwards air force base calif. flight controllers keeping close watch weather 
winds predicted 20 mph gusts 29 mph 
mission rules dictate headwinds exceed 29 mph touchdown 
shuttle carries enough supplies stay orbit three days beyond sunday necessary nasa said 
mission control awakened crew members 1:30 a.m. edt today playing song cosmos 
thanks wake-up music pretty appropriate today said commander loren shriver 
song selected honor hubble space telescope open business said mission control commentator james hartsfield 
astronauts finish experiments photography today pack trip home 
also planned hold news conference space reporters johnson space center 
shriver pilot charles bolden run series tests make certain shuttle computers control jets ready return earth 
crew deployed hubble wednesday stayed close telescope case trouble opening aperture door 
mission specialists bruce mccandless kathryn sullivan prepared perform space walk today crank open orbit 380 miles earth 
proved unnecessary door opened friday morning exposing telescope finely polished 94.5-inch eye starlight first time 
two hubble four position-stabilizing gyroscopes stopped working door opened engineers later got back line 
gyroscopes knocked motion kept telescope safe mode said dave drachlis director orbital verifications goddard 
colleague steve terry said hubble systems set conservative limits first operations caused many malfunctions 
also two communications outages totaling several hours 
hubble designed safe modes master destiny case contact orbiting communications system lost extended periods terry said 
nasa planned release first test image telescope tuesday showing open star cluster constellation carina 
terry said would delayed antenna problems 
scientific data follow month two 
telescope named late astronomer edwin p. hubble enable astronomers 15-year working lifetime look back 14 billion years possibly determine age universe 
universe believed created 15 billion years ago cosmic explosion 
hubble provided basis big-bang theory creation 
discovered 1920s universe expanding farther galaxy earth faster moving away 
