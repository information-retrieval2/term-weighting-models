# Term-weighting models

This repo performs the transformation of texts to mathematical representations (matrices) via term-weighting models used in (Steinberger & Jezek, 2009).