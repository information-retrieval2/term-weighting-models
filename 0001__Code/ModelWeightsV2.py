# coding=utf-8
# !usr/bin/env python
from TABICON37.ManejoArchivos import *
from TABICON37.MyListArgs import *
from TABICON37.MySintaxis import *
from nltk.tokenize import word_tokenize
from nltk import ngrams

"""
try:
    word_tokenize("Texto de ejemplo")
except Exception as e:
    print("Recursos no instalados")
    import nltk 
    nltk.download('punkt')
"""

import multiprocessing as mp
import numpy as np
import operator
import math
import sys
import os

class ModelWeightsV2:
    """
    Esta clase proporciona los metodos necesarios para la transformación de 
    un conjunto de textos de entrada a diferentes modelos de texto. Los modelos
    de texto considerados en este componente están basados en el trabajo de 
    (Steinberger & Jezek, 2009) - Evaluation measures for text summarization.
    Autor: Jonathan Rojas Simón <IDS_Jonathan_rojas@hotmail.com>
    """


    def __init__(self, argv):
        """
        Constructor de la clase que inicializa los parámetros de línea de
        comandos
        """
        args = []
        for element in range(1,len(argv)):
            args.append(argv[element])
        param = MyListArgs(args)

        configFile = param.valueArgsAsString("-CONFIG","")
        if configFile != "":
            param.addArgsFromFile(configFile)

        sintaxis = "-INDIRMW:str -INEXTMW:str -OUTDIRMW:str -OUTEXTMW:str -NGRAM:int -CODE:int -NCORES:int -INV:bool"
        #print(param)
        #MySintaxis(sintaxis, param)

        self.input_dir = param.valueArgsAsString("-INDIRMW","")
        self.input_ext = param.valueArgsAsString("-INEXTMW","")
        self.output_dir = param.valueArgsAsString("-OUTDIRMW","")
        self.output_ext = param.valueArgsAsString("-OUTEXTMW","")
        self.n_gram = param.valueArgsAsInteger("-NGRAM", 1)
        self.g_code = param.valueArgsAsInteger("-CODE", 1)
        self.n_cores = param.valueArgsAsInteger("-NCORES", 1)
        self.inv = param.valueArgsAsBoolean("-INV", False)

        if self.n_cores < 1 or self.n_cores > mp.cpu_count():
            self.n_cores = mp.cpu_count()

        self.mod_l = ["FQ","BI","AU","LO"]  #Modelos de pesado locales
        self.mod_g = ["NW","ISF","GF","EN"] #Modelos de pesado globales
        if self.g_code>(len(self.mod_g)*len(self.mod_l)) or self.g_code<1:
            self.g_code = 1


    def tokenize_per_words(self, content_file):
        """
        Esta función segmenta las oraciones de un texto de entrada por 
        palabra, toma en cuenta el contenido de un documento por oración 
        y lo tokeniza en función a las palabras de la cadena de texto
        :param content_file: es una lista que contiene el texto a procesar
        :return: Regresa una lista de palabras tokenizadas
        """
        list_words = []
        for i in content_file:
            list_words.append(i.split())
        return list_words


    def get_ngrams(self, content_file):
        """
        Función que realiza la extracción de ngramas lineales a partir de una
        lista de elementos segmentados, la longitud de ngramas es determinada
        como parámetro de entrada.
        :param content_file: es una lista del contenido del archivo
        :return: una lista que contiene tuplas de ngramas
        """
        list_ngrams = []
        for i in content_file:
            list_ngrams.append(list(ngrams(i, int(self.n_gram))))
        return list_ngrams


    def get_uniq_ngs(self, content_file):
        """
        Funcion que extrae los ngramas unicos a partir de una secuencia 
        de oraciones obtenidas a partir de un archivo de texto
        :param content_file: es una lista que contiene varias secuencias
        de ngramas
        :return: un conjunto que contiene solo los ngramas únicos
        """
        list_strings = []
        for i in content_file:
            for m in i:
                list_strings.append(m)
        return set(list_strings)


    def get_frequence_term(self, term, sentence):
        """
        Funcion que calcula la frecuencia de un termino definido como 
        parámetro en una oración determinada como parámetro
        :param term: es el elemento a calcular su frecuencia en la oración 
        sentence
        :return: la frecuencia de termino term en la oracion sentence
        """
        freq = 0.0
        for elem in sentence:
            if term==elem:
                freq+=1.0
        return freq


    def get_binary_weight(self, term, sentence):
        """
        Funcion que calcula el peso de un termino definido como parámetro de 
        forma binaria (si el termino term aparece en la oracion sentence,
        entonces retornará 1, en caso contrario aparece 0)
        :param term: es el elemento a calcular su frecuencia en la oración 
        sentence
        :return: 1 si term aparece en la oración sentence, 0 en caso contrario
        """
        for elem in sentence:
            if term==elem:
                return 1.0
        return 0.0


    def get_frequence_term_max(self, sentence):
        """
        Función que devuelve el número de ocurrencias del término de mayor 
        frecuencia a partir de una lista de términos determinado como parámetro
        :param sentence: es la lista de los términos determinados como oración
        :return: la frecuencia del término de mayor repetición de valores
        """
        dict_terms = dict.fromkeys(list(sentence),0.0)
        for term in sentence:
            dict_terms[term] += 1.0
        return max(dict_terms.values())


    def get_augmented_weight(self, term, sentence):
        """
        Función que devuelve el pesado de término term aumentado de la oración
        sentence. Este método establece la relación de la frecuencia del 
        término term con respecto a la frecuencia del término de mayor 
        frecuencia de la oracion sentence.
        :param sentence: es la lista de los términos determinados como oración
        :return: la frecuencia del término de forma aumentada
        """
        return (0.5 + 0.5 * float(self.get_frequence_term(term,sentence))/float(self.get_frequence_term_max(sentence)))


    def get_logarithmic_weight(self, term, sentence):
        """
        Función que devuelve el pesado local logarítmico de un término term a
        partir de una oración sentence.
        :param sentence: es la lista de los términos que conforman el contenido
        de una oración
        :return: la frecuencia del término a través de la función logarítmica
        """
        return math.log10(1.0+self.get_frequence_term(term, sentence))


    def get_local_sch(self, terms, content, option):
        """
        Esta función calcula el pesado local tomando en cuenta las siguientes
        opciones:
         - FQ -> Frecuencia de término
         - BI -> Pesado binario
         - AU -> Pesado aumentado
         - LO -> Pesado logarítmico
        :param terms: es el vector único de términos del documento asociado
        :param content: es el vector que representa el contenido textual del 
        documento
        :param option: es la opción que representa al pesado de término a utilizar
        """
        local_sch = []
        for term in terms:
            term_w = []
            for inter_s in content:
                if option=="FQ":
                    term_w.append(self.get_frequence_term(term, inter_s))
                elif(option=="BI"):
                    term_w.append(self.get_binary_weight(term, inter_s))
                elif(option=="AU"):
                    term_w.append(self.get_augmented_weight(term, inter_s))
                elif(option=="LO"):
                    term_w.append(self.get_logarithmic_weight(term, inter_s))
            local_sch.append(term_w)
        return local_sch


    def get_no_weight_global(self):
        """
        Función utilizada para especificar que no hay esquemas de pesado 
        global en particular.
        :return: el valor de 1
        """
        return 1.0


    def get_sent_freq_term(self, term, sentences):
        """
        Función que calcula el número de oraciones en donde el termino 
        term aparece
        :param term: es el término en cuestion a calcular
        :param sentences: es una secuencia de oraciones donde cada oración 
        contiene una lista de términos inluidos
        :return: el número de oraciones donde el termino term aparece
        """
        freq_sent_term = 0.0
        for sent in sentences:
            for termi in set(sent):
                if term==termi:
                    freq_sent_term += 1.0
        return freq_sent_term


    def get_global_freq_term(self, term, sentences):
        """
        Función que calcula el número de veces en donde el termino 
        term aparece en todo el documento
        :param term: es el término en cuestion a calcular
        :param sentences: es una secuencia de oraciones donde cada oración 
        contiene una lista de términos inluidos, además esta lista es 
        considerada como todo el documento
        :return: el número de veces donde el termino term aparece a través de 
        todo el documento
        """
        freq_sent_term = 0.0
        for sent in sentences:
            for termi in sent:
                if term==termi:
                    freq_sent_term += 1.0
        return freq_sent_term


    def get_inverse_sentence_frequency(self, term, sentences):
        """
        Función que calcula el pesado de un término a través de la frecuencia 
        inversa de la oración, el cual establece la relación entre el número
        de oraciones de un documento y el número de oraciones donde se 
        encuentre el término term.
        :param term: es el término en cuestion a calcular su frecuencia a 
        través del documento
        :param sentences: es una secuencia de oraciones donde cada oración 
        contiene una lista de términos inluidos
        :return: el valor de la frecuencia inversa del término en las oraciones
        """
        n_sentences = len(sentences)
        freq_global_term = self.get_sent_freq_term(term,sentences)
        return (math.log10(float(n_sentences)/float(freq_global_term))+1.0)


    def get_GFIDF(self, term, sentences):
        """
        Función que calcula la relación entre el número de oraciones que 
        contiene el término term, y el número de veces que el término term
        es frecuente en todo el documento. Este esquema de pesado global de 
        términos es conocido como GFIDF.
        :param term: es el término en cuestion a calcular su frecuencia a 
        través del documento
        :param sentences: es una secuencia de oraciones donde cada oración 
        contiene una lista de términos inluidos
        :return: el valor GFIDF de l término term en todo el documento
        """
        gfi = self.get_global_freq_term(term,sentences)
        sfi = self.get_sent_freq_term(term,sentences)
        return(float(gfi)/float(sfi))


    def get_entropy_frequency(self, term, sentences):
        """
        Función que calcula el pesado global de un termino a través de la
        frecuencia de entropía, el cual determina el nivel de información de
        un termino a través de un documento.
        :param term: es el término en cuestion a calcular su frecuencia a 
        través del documento
        :param sentences: es una secuencia de oraciones donde cada oración 
        contiene una lista de términos inluidos
        :return: el valor GFIDF de l término term en todo el documento
        """
        gfi = float(self.get_global_freq_term(term, sentences))
        if len(sentences) > 1:
            n_sentences = len(sentences)
        else:
            n_sentences = 1.0001
        acum_en = 0.0
        for sent in sentences:
            tfij = float(self.get_frequence_term(term, sent))
            if tfij == 0.0:
                tfij = float(self.get_global_freq_term(term,sentences)) / float(n_sentences)
            pij = float(tfij/gfi)
            acum_en += ((math.log10(pij)*pij)/math.log10(n_sentences))
        return (1.0-acum_en)


    def get_global_sch(self, terms, content, option):
        """
        Esta función calcula el esquema de pesado global tomando en cuenta las 
        siguientes opciones:
         - NW  -> Sin pesado (No weight)
         - ISF -> Frecuencia Inversa de la Oración (Inverse Sentence Frequency)
         - GF  -> GFIDF
         - EN  -> Pesado por entropía (Entropy frequency)
        :param terms: es el vector único de términos del documento asociado
        :param content: es el vector que representa el contenido textual del 
        documento
        :param option: es la opción que representa al pesado de término a 
        utilizar
        """
        global_sch = []
        for term in terms:
            term_w = []
            for inter_s in content:
                if option=="NW":
                    term_w.append(self.get_no_weight_global())
                elif(option=="ISF"):
                    term_w.append(self.get_inverse_sentence_frequency(term, content))
                elif(option=="GF"):
                    term_w.append(self.get_GFIDF(term, content))
                elif(option=="EN"):
                    term_w.append(self.get_entropy_frequency(term, content))
            global_sch.append(term_w)
        return global_sch


    def get_comb_matrix(self, matrix_l, matrix_g):
        """
        Esta función realiza la multiplicación del esquema de pesado local con 
        el esquema de pesado global. Esta multiplicación permite realizar la 
        combinación de modelos de texto locales y modelos de texto globales.
        Esta representación puede ser útil cuando se desean combinar varios 
        esquemas de pesado.
        :param matrix_l: Es el esquema de pesado local
        :param matrix_g: Es el esquema de pesado global
        :return: Un esquema combinado del pesado local y pesado global
        """
        out_m = []
        for s in range(len(matrix_l)):
            new_s = []
            for t in range(len(matrix_l[s])):
                new_s.append(float(matrix_l[s][t])*float(matrix_g[s][t]))
            out_m.append(new_s)
        return out_m


    def get_total_terms(self, list_content):
        """
        Esta función realiza el conteo total de terminos incluidos de un 
        documento de texto. Esta función es útil cuando se desee conocer el 
        número total de terminos (palabras, bigramas, trigramas, etc.) de un
        conjunto de dosumentos tomados como entrada. Además, para el LSA es
        necesario conocer este número para realizar la reducción de dimensiones
        adecuado.
        :param list_content: Es una lista general compuesta de oraciones donde 
        cada oración contiene una lista de varios terminos considerados.
        :return: El número total de elementos que tiene un documento de texto
        """
        count_elems = 0
        for sent in list_content:
            count_elems += len(sent)
        return count_elems


    def calculate_matrix(self, filename, path_name, io):
        list_content = io.read_text_file_nonull(path_name)
        new_file = filename.replace(self.input_ext,self.output_ext)
        out_file = io.add_to_path(self.output_dir, new_file)
        print(new_file)

        if list_content == []: # Si el archivo de entrada está vacío
            io.write_string_file(out_file,"") # Escribe un vacío en el archivo
            print("Archivo vacío")
        else:
            list_content = self.tokenize_per_words(list_content)

            # Generación de n-gramas
            list_content = self.get_ngrams(list_content)
            uniq_ngs = list(self.get_uniq_ngs(list_content))
            uniq_ngs.sort()

            coord_x = ((self.g_code-1)%len(self.mod_l)) # Coordenada X
            coord_y = ((self.g_code-1)//len(self.mod_l))# Coordenada Y

            model_l = self.mod_l[coord_x] # Selección del pesado local
            model_g = self.mod_g[coord_y] # Selección del pesado global

            # Obtención de esquemas de pesado
            local_schema = self.get_local_sch(uniq_ngs, list_content, model_l)
            global_schema = self.get_global_sch(uniq_ngs,list_content, model_g)
            output_matrix = self.get_comb_matrix(local_schema, global_schema)

            if self.inv: 
                output_matrix = np.array(output_matrix).transpose()

            # Obtención del número total de terminos (tokens)
            cad_out = "TOKENS " + str(len(uniq_ngs)) + "\n"
            # Obtención de dimensiones (oraciones)
            cad_out += "SENTENCES " + str(len(list_content)) + "\n"
            # Obtención de gramas
            out_grams = []
            for cad_t in uniq_ngs:
                cad_iter = ""
                for cad_e in cad_t:
                    cad_iter += cad_e + " "
                out_grams.append(cad_iter)
            cad_out += "@SPLIT@ ".join(out_grams) + "\n"

            # Despliegue de la matriz de salida en el archivo
            for elem_s in output_matrix:
                out_w = []
                for term_w in elem_s:
                    out_w.append(str(float(term_w)))
                cad_out += ",".join(out_w) + "\n"

            io.write_string_file(out_file,cad_out) # Escritura del archivo


if __name__ == "__main__":

    ow = ModelWeightsV2(sys.argv)
    io = ManejoArchivos()
    #print("Number of processors: ", mp.cpu_count())

    list_in = io.list_files(ow.input_dir, ow.input_ext)

    io.until_crear_carpetas(ow.output_dir)
    dict_files = {}

    for name_file in list_in:
        p_in = io.add_to_path(ow.input_dir, name_file)
        dict_files[(name_file, p_in, io, )] = os.stat(p_in).st_size

    res_list = sorted(dict_files.items(), key=operator.itemgetter(1))

    while len(res_list) != 0:
        if len(res_list) < ow.n_cores:
            ow.n_cores = len(res_list)
        inter_task = [res_list[i_t] for i_t in range(ow.n_cores)]
        processes = [mp.Process(target=ow.calculate_matrix, args=ta[0]) for ta in inter_task]
        [process.start() for process in processes]
        [process.join() for process in processes]
        for item in inter_task:
            res_list.remove(item)